FROM golang:alpine

MAINTAINER Coşku BAŞ "cosku.bas@gmail.com"

ENV SS_PORT 8488
ENV SS_CYPHER aes-128-cfb
ENV SS_PASSWORD YOUR_PASS

RUN apk --update --no-cache add git
RUN go get -u github.com/shadowsocks/go-shadowsocks2
RUN apk del git
RUN rm -rf /go/src

CMD ["/go/bin/go-shadowsocks2", "-verbose", "-s", "ss://$SS_CYPHER:$SS_PASSWORD@:$SS_PORT"]
